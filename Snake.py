# -*- encoding: utf-8 -*-
__author__ = 'Tadej'

from sys import version
import random

if version.startswith('2'):
    from Tkinter import *
    from Queue import Queue
else:
    from tkinter import *
    from queue import Queue


def random_smer():
    return random.choice(((1, 0), (-1, 0), (0, 1)))


class Snake():
    def __init__(self, master):
        """VSE"""
        # MENU
        menu = Menu(master)
        master.config(menu=menu)  # Dodamo menu

        file_menu = Menu(menu, tearoff=0)
        menu.add_cascade(label="File", menu=file_menu)
        file_menu.add_command(label="(E) Debug Mode (processor hungry)", command=self.debug)
        file_menu.add_command(label="(R) Restart", command=self.restart)
        file_menu.add_separator()
        file_menu.add_command(label="(Q) Exit", command=self.destroy)
        mode_menu = Menu(menu, tearoff=0)
        menu.add_cascade(label="Mode", menu=mode_menu)
        mode_menu.add_command(label="(F1) Player - Easy", command=self.player_human_easy)
        mode_menu.add_command(label="(F2) Player - Medium", command=self.player_human_medium)
        mode_menu.add_command(label="(F3) Player - Hard", command=self.player_human_hard)
        mode_menu.add_command(label="(F4) Player - Insane", command=self.player_human_insane)
        mode_menu.add_command(label="(F5) Computer 1 (Kapitan Kotnik - nedokoncan)", command=self.player_AI1)
        mode_menu.add_command(label="(F6) Computer 2 (Leejev Algoritem)", command=self.player_AI2)
        # MENU/

        # celice
        self.margin = 5
        self.cell = 30
        self.cols, self.rows = 40, 20  # 40, 20
        self.brick = self.cols*self.rows
        self.canvasWidth = 2 * self.margin + self.cols * self.cell + 2 * self.cell
        self.canvasHeight = 2 * self.margin + self.rows * self.cell + 2 * self.cell
        self.canvas = Canvas(master, width=self.canvasWidth, height=self.canvasHeight)
        self.canvas.pack()
        # celice/

        root.bind("<Key>", self.input)

        # parametri
        self.board = []
        self.smer = random_smer()
        self.xf, self.yf = 0, 0
        self.debugMode, self.gameOver = False, False
        self.head_x, self.head_y = self.canvasWidth / 2, self.canvasHeight / 2
        self.lenght = 3
        self.score = 0
        self.speed = 15
        self.prevSpeed = self.speed
        self.scoreBest = self.score
        self.player = "AI2"
        self.snake = []
        self.pathXblocked, self.pathYblocked = False, False
        self.changeSmer = False
        self.gotFood = False
        self.prevSmer = None
        # parametri/

        # klic funkcij
        self.load_board()  # Nardi self.cols x self.rows matriko
        self.redraw()
        # klic funkcij/

    ########## menu funkcije ##########

    def debug(self):
        self.debugMode = not self.debugMode

    def destroy(self):
        self.canvas.master.destroy()

    def player_human_easy(self):
        self.player = "human"
        self.speed = 150
        self.prevSpeed = self.speed
        self.restart()

    def player_human_medium(self):
        self.player = "human"
        self.speed = 100
        self.prevSpeed = self.speed
        self.restart()

    def player_human_hard(self):
        self.player = "human"
        self.speed = 60
        self.prevSpeed = self.speed
        self.restart()

    def player_human_insane(self):
        self.player = "human"
        self.speed = 30
        self.prevSpeed = self.speed
        self.restart()

    def player_AI1(self):
        self.player = "AI1"
        self.speed = 15
        self.prevSpeed = self.speed
        self.restart()

    def player_AI2(self):
        self.player = "AI2"
        self.speed = 15
        self.prevSpeed = self.speed
        self.restart()

    def restart(self):
        self.board = []
        self.smer = random_smer()
        self.gameOver = False
        self.changeSmer = False
        self.score = 0
        self.head_x, self.head_y = 0, 0
        self.load_board()

    ########## menu funkcije/ ##########
    ########## risanje celic ##########

    def load_board(self):
        """Nardi self.cols x self.rows matriko, poklice samo nazacetku igre"""
        self.board = []
        self.board += [[self.brick] * (self.cols + 2)]
        for i in range(self.rows):
            self.board += [[self.brick] + [0] * self.cols + [self.brick]]
        self.board += [[self.brick] * (self.cols + 2)]
        for i in range(self.lenght):
            self.board[int(self.rows / 2)][int(self.cols / 2) - i] = len(range(self.lenght)) - i
        self.head()
        self.food()

    def redraw(self):
        """rise spremembe"""
        # self.board[gor/dol][levo/desno]
        if self.gameOver:
            if self.speed < 200:
                self.speed = 200
            if self.scoreBest < self.score:
                self.scoreBest = self.score
            self.canvas.create_text(self.canvasWidth / 3.5, self.canvasHeight / 1.25,
                                    text="Game Over!\n"
                                         "Your score was: "+str(self.score)+"\n"
                                         "Best score this session: "+str(self.scoreBest)+"\n",
                                    font=("Times New Roman", 20, "bold"))
        else:
            if self.speed == 200:
                self.speed = self.prevSpeed
            if self.player == "AI1":
                self.AI1()
            elif self.player == "AI2":
                self.leeAI2()
            self.move()
            self.canvas.delete(ALL)
            self.snake = []
            self.draw_board()
        self.canvas.after(self.speed, self.redraw)  # milisekunde

    def draw_board(self):
        """na vsaki celici poklice nasledno funkcijo"""
        for i in range(self.rows + 2):
            for j in range(self.cols + 2):  # vsako celico posebej
                self.draw_cell(i, j)

    def draw_cell(self, x, y):
        """za celico pogleda vrednost in primerno narise/obarva"""
        left, top = self.margin + y * self.cell, self.margin + x * self.cell
        right, bottom = left + self.cell, top + self.cell
        if self.debugMode:  # narise prazne celice
            self.canvas.create_rectangle(left, top, right, bottom, fill="white")
        if self.board[x][y] > 0:  # narise celice za kaco ( pozitivna celica )
            if self.board[x][y] < self.brick:
                self.snake.append((x, y))
                num = self.board[x][y] * 2
                if num < 16:
                    num = hex(num)[2:]
                    num = "0" + num
                elif num <= 255:
                    num = hex(num)[2:]
                else:
                    num = "ff"
                html_code = "#0066" + str(num)
                self.canvas.create_rectangle(left, top, right, bottom, fill=html_code)
            else:
                self.canvas.create_rectangle(left, top, right, bottom,
                                             fill="#C0C0C0",
                                             width=2,
                                             outline="#808080")
        elif self.board[x][y] == -self.brick:  # hrana ( negativna celica )
            self.canvas.create_oval(left, top, right, bottom,
                                    fill="#FF0000",
                                    width=2,
                                    outline="#8B0000")
            # HD apple
            self.canvas.create_oval(left + 18, top - 1, right - 18, bottom - 20,
                                    fill="#000000",
                                    width=2,
                                    outline="#8B4513")
        if self.debugMode:  # cell priority number
            self.canvas.create_text(left + self.cell / 2, top + self.cell / 2,
                                    text=str(self.board[x][y]),
                                    font=("Times New Roman", 10, "bold"))
        elif x == 0 and y == 0:
            self.canvas.create_text(left + self.cell / 2, top + self.cell / 2,
                                    text=str(self.score),
                                    font=("Times New Roman", 14, "bold"),
                                    fill="#006600")

    ########## risanje celic/ ##########

    def input(self, e):
        if e.char == "e" or e.char == "E":
            self.debugMode = not self.debugMode
        elif e.char == "q" or e.char == "Q":
            self.canvas.master.destroy()
        elif e.char == "r" or e.char == "R":
            self.restart()
        elif e.keysym == "F1":
            self.player_human_easy()
        elif e.keysym == "F2":
            self.player_human_medium()
        elif e.keysym == "F3":
            self.player_human_hard()
        elif e.keysym == "F4":
            self.player_human_insane()
        elif e.keysym == "F5":
            self.player_AI1()
        elif e.keysym == "F6":
            self.player_AI2()
        if not self.gameOver and self.player == "human" and not self.changeSmer:
            if (e.keysym == "Up" or e.keysym == "w" or e.keysym == "W" or e.keysym == "KP_8") and self.smer[1] != 0:
                self.smer = (-1, 0)
                self.changeSmer = True
            elif (e.keysym == "Down" or e.keysym == "s" or e.keysym == "S" or e.keysym == "KP_2") and self.smer[1] != 0:
                self.smer = (1, 0)
                self.changeSmer = True
            elif (e.keysym == "Left" or e.keysym == "a" or e.keysym == "A" or e.keysym == "KP_4") and self.smer[0] != 0:
                self.smer = (0, -1)
                self.changeSmer = True
            elif (e.keysym == "Right" or e.keysym == "d" or e.keysym == "D" or e.keysym == "KP_6") and self.smer[0] != 0:
                self.smer = (0, 1)
                self.changeSmer = True

    ########## engine ##########

    def move(self):
        """Kje bo kaca ce se premakne"""
        dy, dx = self.head_y + self.smer[0], self.head_x + self.smer[1]
        if self.board[dy][dx] > 0:  # zabije vase ali zid
            self.gameOver = True
        elif self.board[dy][dx] == -self.brick:  # prehranjevanje
            self.board[dy][dx] = 1 + self.board[self.head_y][self.head_x]
            self.head_y, self.head_x = dy, dx
            self.score += 1
            self.food()
        else:  # premakni kaco
            self.board[dy][dx] = 1 + self.board[self.head_y][self.head_x]
            self.head_x, self.head_y = dx, dy
            self.tail()
        self.changeSmer = False

    def head(self):
        """najde od kace glavo, poklice samo nazacetk igre"""
        x, y = 1, 1
        for i in range(self.rows):
            i += 1
            for j in range(self.cols):
                j += 1
                if self.board[y][x] < self.board[i][j] < self.brick:  # poisce najvecjo vrednost (razn zid - self.brick)
                    x, y = j, i
        self.head_x, self.head_y = x, y

    def tail(self):
        """zniza vrednost pozitivnih celic do self.brick, zvisa vrednost negativnih celic do -self.brick"""
        for i in range(self.rows):
            i += 1
            for j in range(self.cols):
                j += 1
                if 0 < self.board[i][j] < self.brick:
                    self.board[i][j] -= 1
                elif 0 > self.board[i][j] > -self.brick:
                    self.board[i][j] += 1

    def food(self):
        """hrana na random lokaciji"""
        if self.player == "AI1":
            self.gotFood = True
        emptyCells = []
        for i in range(self.rows):
            i += 1
            for j in range(self.cols):
                j += 1
                if self.board[i][j] == 0:
                    emptyCells.append((i, j))
        if len(emptyCells) > 0:
            randChoice = random.choice(emptyCells)
            x, y = randChoice[1], randChoice[0]
            self.board[y][x] = -self.brick
            self.xf, self.yf = x, y
        else:
            "Si zmagal, al neki"

    ########## engine/ ##########
    ########## AI1 (kapitan kotnik) ##########

    # self.board[gor/dol][levo/desno]

    def row_free(self):
        """funkcija vrne True, ce je vrstica kjer ima kaca glavo prazna.
        (ali je možno pobrati hrano brez da se zaleti vase ali v zid)"""
        for i in self.snake:
            if i[1] > 1:
                if self.head_y == i[0]:
                    if self.yf < i[0] and self.xf < i[1]:
                        if i[1] - self.head_x < self.board[i[0]][i[1]] + 2:  # ce je hrana pred repam
                            return False
                    else:
                        if i[1] - self.head_x < self.board[i[0]][i[1]] + 1:
                            return False
        return True

    def col_free(self):
        """enako kot zgoraj, le da pogleda stolpec"""
        for i in self.snake:
            if i[0] > 1:
                if self.head_x == i[1]:
                    if self.xf < i[1] and self.yf < i[0]:
                        if i[0] - self.head_y < self.board[i[0]][i[1]] + 2:  # ce je hrana pred repam
                            return False
                    else:
                        if i[0] - self.head_y < self.board[i[0]][i[1]] + 1:
                            return False
        return True

    def AI1(self):
        if self.gotFood:
            self.move_back()
        else:
            self.get_food()

    def move_back(self):
        """nazaj na zacetek ((1, self.rows) - levo spodaj)"""
        to_last_row = self.rows - self.head_y
        if self.head_y == 1 and self.head_x == 1:  # kot levo zgorej
            self.smer = (0, 1)
        elif self.head_y < self.rows:  # ce je kaca nad zadnjo vrsto
            if to_last_row > self.board[self.rows][self.head_x]:  # ce se da do zandje vrste pridt
                if self.smer == (-1, 0):
                    self.smer = (0, 1)
                else:
                    self.smer = (1, 0)
        elif self.board[self.head_y+1][self.head_x] == self.brick and self.board[self.head_y][self.head_x-1] == self.brick:   # kot levo spodej
            self.smer = (-1, 0)
            self.gotFood = False
        elif self.board[self.head_y+1][self.head_x] == self.brick:  # spodej
            self.smer = (0, -1)

    def get_food(self):
        """drzi se leve dokler ni na isti visini kakor hrana"""
        if self.head_y == self.rows:
            if self.head_x == 1:  # kot levo spodej
                self.smer = (-1, 0)
            elif self.head_x == self.cols:  # kot desno spodej
                self.smer = (0, -1)
        elif self.yf == self.rows and self.head_y == 1:  # ce je hrana spodej
            if self.head_x == 1:
                self.smer = (0, 1)
            elif self.head_x == self.xf:
                self. smer = (1, 0)
        elif self.yf == self.head_y and self.head_x == 1 and self.row_free():  # ce je hrana zgorej in je row frej
            self.smer = (0, 1)
        elif self.head_y == 1:
            if self.head_x == 1:  # kot levo zgorej
                self.smer = (0, 1)
            elif self.head_x == self.xf and self.col_free():  # zgorej
                self.smer = (1, 0)
            elif self.head_x == self.cols:  # kot desno zgorej
                self.smer = (1, 0)

    ########## AI1/ ##########
    ########## AI2 (Lee) ##########

    def matrika_m(self, v):
        """naredi matriko kjer so prazna polja = v, kača = zid (self.brick)"""
        mat = []
        for i in self.board:
            s = []
            for j in i:
                if j == 0:
                    j = v
                elif 0 < j < self.brick:
                    j += self.brick
                s.append(j)
            mat.append(s)
        return mat

    def leeAI2(self):
        """Leejev algoritem"""
        s = (self.yf, self.xf)  # s je "source"
        d = (self.head_y, self.head_x)  # d je "destination"
        m = self.matrika_m(-1)  # m = matrika samih -1 ista velikost kot polje
        q = Queue()
        q.put(s)  # najprej je v vrsti za obdelavo samo s (to je lahko kar seznam)
        m[self.yf][self.xf] = 0
        lee = False
        while not q.empty():
            c = q.get(0)
            y, x = c[0], c[1]
            sosedi = [(y, x+1), (y-1, x), (y, x-1), (y+1, x)]
            for j, i in sosedi:
                if m[j][i] < 0:  # tam ni kace
                    if m[j][i] == -1:  # (j, i) se nismo obdelali
                        m[j][i] = 1 + m[c[0]][c[1]]
                        q.put((j, i))  # dodaj (j, i) na konec q
                if j == self.head_y and i == self.head_x:  # prisli smo do konca
                    lee = True
                    self.lee_path(d, m)
        if not lee:
            self.zidovi()

    def lee_path(self, d, m):
        """pogleda katera sosednja celica ima NAJMANJSO vrednost in primerno spremeni smer"""
        y, x = d[0], d[1]
        sosedi = [(y, x+1), (y-1, x), (y, x-1), (y+1, x)]  # [desno, gor, levo, dol]
        s_vrednosti = [m[l[0]][l[1]] for l in sosedi if -1 < m[l[0]][l[1]] < self.brick]
        mini = min(s_vrednosti)
        if m[y][x+1] == mini and self.smer != (0, -1):
            self.smer = (0, 1)
        elif m[y-1][x] == mini and self.smer != (1, 0):
            self.smer = (-1, 0)
        elif m[y][x-1] == mini and self.smer != (0, 1):
            self.smer = (0, -1)
        elif m[y+1][x] == mini and self.smer != (-1, 0):
            self.smer = (1, 0)

    ########## KO JE ZAPRT ##########

    def zidovi(self):
        """poišče kačo/zid"""
        glava = (self.head_y, self.head_x)
        m = self.matrika_m(99)  # prazna poja so enaka 99
        q = Queue()
        q.put(glava)
        zidovi = Queue()
        zeUporablen = []
        notranjost = []
        while not q.empty():
            c = q.get(0)
            y, x = c[0], c[1]
            sosedi = [(y, x+1), (y-1, x), (y, x-1), (y+1, x)]
            for j, i in sosedi:
                if m[j][i] > self.brick and (j, i) not in zeUporablen:
                    zidovi.put((j, i))
                    q.put((j, i))
                    if m[j][i] != m[self.head_y + self.smer[0]][self.head_x + self.smer[1]]:
                        zeUporablen.append((j, i))
                if m[j][i] < self.brick and (j, i) not in zeUporablen:
                    notranjost.append((j, i))
                    zeUporablen.append((j, i))
        self.boxed_in(zidovi, notranjost)

    def boxed_in(self, zidovi, notranjost):
        """podobno kot leejev algoritem. V nekaterih primerih deluje dobro, v nekaterih pa slabo"""
        zeUporablen = []
        m = self.matrika_m(99)  # prazna poja so enaka 99
        q = Queue()
        i = 0
        while not zidovi.empty():
            i += 1
            c = zidovi.get(0)
            y, x = c[0], c[1]
            sosedi = [(y, x+1), (y-1, x), (y, x-1), (y+1, x)]
            for j, i in sosedi:
                if m[j][i] == 99 and (j, i) not in zeUporablen and (j, i) in notranjost:
                    m[j][i] -= i
                    q.put((j, i))
                    zeUporablen.append((j, i))
        while not q.empty():
            c = q.get(0)
            y, x = c[0], c[1]
            sosedi = [(y, x+1), (y-1, x), (y, x-1), (y+1, x)]
            for j, i in sosedi:
                if m[y][x] <= m[j][i] <= 99 and (j, i) not in zeUporablen and (j, i) in notranjost:
                    m[j][i] = m[y][x] - 1
                    q.put((j, i))
                    zeUporablen.append((j, i))
        self.boxed_path(m)

    def boxed_path(self, m):
        """pogleda katera sosednja celica ima NAJVECJO vrednost in primerno spremeni smer"""
        g = (self.head_y, self.head_x)
        y, x = g[0], g[1]
        sosedi = [(y, x+1), (y-1, x), (y, x-1), (y+1, x)]
        s_vrednosti = [m[l[0]][l[1]] for l in sosedi if 0 < m[l[0]][l[1]] < self.brick]
        # print(m)
        # print(sosedi)
        # print(s_vrednosti)
        # print("desno", "gor", "levo", "dol")
        if len(s_vrednosti) != 0:
            maxi = max(s_vrednosti)
            if m[y + self.smer[0]][x + self.smer[1]] == maxi:
                pass
            elif m[y][x+1] == maxi:
                self.smer = (0, 1)
            elif m[y-1][x] == maxi:
                self.smer = (-1, 0)
            elif m[y][x-1] == maxi:
                self.smer = (0, -1)
            elif maxi == m[y+1][x]:
                self.smer = (1, 0)
        else:  # Neumen AI
            if self.prevSmer != None:
                if self.board[self.head_y+self.prevSmer[0]][self.head_x+self.prevSmer[1]] <= 0:
                    self.smer = self.prevSmer
                self.prevSmer = None
            elif self.board[self.head_y+self.smer[0]][self.head_x+self.smer[1]] > 0:
                self.AI0()

    ########## AI2/ ##########

    def AI0(self):
        """Neumen AI"""
        x, y = self.head_x, self.head_y
        up = self.board[y-1][x]
        down = self.board[y+1][x]
        left = self.board[y][x-1]
        right = self.board[y][x+1]
        mini = min(up, down, left, right)
        self.prevSmer = self.smer
        m = ()
        for i in self.prevSmer:
            m += (-i,)
        self.prevSmer = m
        if mini == up and self.smer != (1, 0) and self.smer != (-1, 0):
            self.smer = (-1, 0)
        elif mini == down and self.smer != (-1, 0) and self.smer != (1, 0):
            self.smer = (1, 0)
        elif mini == left and self.smer != (0, 1) and self.smer != (0, -1):
            self.smer = (0, -1)
        elif mini == right and self.smer != (0, -1) and self.smer != (0, 1):
            self.smer = (0, 1)

root = Tk()
root.title("Snake")
aplikacija = Snake(root)
root.mainloop()