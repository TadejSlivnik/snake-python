##### Avtor: Tadej Slivnik #####

# README #

Repozitorij vsebuje projekt pri predmetu Programiranje 2. Napisan je v programskem jeziku Python.

### Opis igre ###

Kača/Snake je videoigra, ki izvira iz leta 1976. V igri vodite kačo s tipkami za krmarjenje (gor, dol, levo, desno ali W,S,A,D), ki s pobiranjem hrane raste in pridobiva točke. Pazite, da se ne zaletite v rob ali v samega sebe.

Umetna inteligeca je narejena s pomočjo Leejevega algoritma:

![IMAGE](http://suvitruf.ru/wp-content/uploads/2012/05/Lee_algorithm.png)

### Zahteve ###

* Python, verzijo 3.x ali 2.x
* Program se požene z datoteko Snake.py

(Uporabnik ne potrebuje knjižnic, ki niso del standardne distribucije pythona.)

### Kontakt ###

* tadej.slivnik@student.fmf.uni-lj.si
* slivnik92@outlook.com